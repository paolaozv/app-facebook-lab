import React, { Component } from 'react';
import Post from '../Post';

export default class App extends Component {

	constructor(props) {
    super(props);
    this.state = {
			post: '',
			groupPost: [
        {description: 'Hola, cómo están?', id: 0},
				{description: 'Hola, cómo están?', id: 1},
				{description: 'Hola, cómo están?', id: 2},
      ],
    }
    this.addPost = this.addPost.bind(this);
    this.onChange = this.onChange.bind(this);
    this.delete = this.delete.bind(this);
	}
	
	onChange(e) {
    this.setState({
			post: e.target.value,
    });
	}
	
	addPost() {
    const {
			post,
			groupPost
    } = this.state;
    
    this.setState({
      groupPost: groupPost.concat({id: groupPost.length, description: post}), 
      post: '',
    });
	}
	
	delete(id) {
    this.setState({
      groupPost: this.state.groupPost.filter(group => group.id !== id),
    });
  }

	render() {
		const {
			groupPost,
			post
		} = this.state;
    return (
      <div>
				<textarea value={post} onChange={this.onChange} className="form-control" placeholder="What are you doing right now?"></textarea>
				<br/>
				<ul className="nav nav-pills pull-left ">
					<li><a title="" data-toggle="tooltip" data-placement="bottom" data-original-title="Audio"><i className="glyphicon glyphicon-bullhorn"></i></a></li>
					<li><a title="" data-toggle="tooltip" data-placement="bottom" data-original-title="Video"><i className=" glyphicon glyphicon-facetime-video"></i></a></li>
					<li><a title="" data-toggle="tooltip" data-placement="bottom" data-original-title="Picture"><i className="glyphicon glyphicon-picture"></i></a></li>
				</ul>
				<button onClick={this.addPost} className="btn btn-success pull-right">Compartir</button>
				<div>
					{
						groupPost.map((item) => 
							<Post item={item} key={item.id} delete={this.delete} />
						)
					}
				</div>
			</div>
    );
  }
}
