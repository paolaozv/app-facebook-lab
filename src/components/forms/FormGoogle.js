import React, { Component } from 'react';

class FormGoogle extends Component {
	constructor(props) {
		super(props);
		this.handleAuth = this.handleAuth.bind(this);
	}

	handleAuth() {
		const firebase = this.props.firebase;
		const provider = new firebase.auth.GoogleAuthProvider();
		firebase.auth().signInWithPopup(provider)
			.then(result => console.log(`${result.user.email} ha iniciado sesion`))
			.catch(error => console.log(`Error ${error.code}: ${error.message}`));
	}

  render() {
    return (
      <div>
				<button onClick={this.handleAuth}
					className="btn btn-lg btn-primary btn-block btn-signin">
					Login con Google</button>
      </div>
    );
  }
}

export default FormGoogle;
