import React, { Component } from 'react';
import Input from '../Input';

export default class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      email: '', password: '',
      messageEmail: '', messagePassword: ''
    }

    this.onChange = this.onChange.bind(this);
    this.handleLogin = this.handleLogin.bind(this);
  }

  onChange(e) {
    this.setState({
      [e.target.name]: e.target.value,
    })
  }

  handleLogin(e) {
    var _this = this;

    fetch('url', {
      method: 'POST',
      body: JSON.stringify({
        email: this.state.email,
        password: this.state.password
      })
    })
    .then(function(response){
      if( response.ok ) {
        response.json().then( res => {

          _this.setState({ messageEmail: '', messagePassword: '' })

          if( res.status !== 'error' ){
            console.log('auth_token: ', res.auth_token);
            this.props.history.push({
              pathname: '/page'
            });
          } else {
            _this.setState({ messageEmail: res.message.email[0],
            messagePassword: res.message.password[0] });
            this.props.history.push({
              pathname: '/page'
            });
          }
        });
      }
    })
    .catch(function(err){
      console.log(err);
    });
  }

  render() {
    const {
      email,
      password,
      messageEmail,
      messagePassword
    } = this.state;
    return (
      <div>
        <form>
          <div className="input-group input-group-lg">
            <span className="input-group-addon">
              <i className="glyphicon glyphicon-envelope" />
            </span>
            <Input className="form-control"
              type="email"
              name="email"
              placeholder="Email"
              onChange={this.onChange}
              value={email}
            />
            <br/>
            <span>{messageEmail}</span>
          </div>
          <div className="input-group input-group-lg">
            <span className="input-group-addon">
              <i className="glyphicon glyphicon-lock" />
            </span>
            <Input className="form-control"
              type="password"
              name="password"
              placeholder="Contraseña"
              onChange={this.onChange}
              value={password}
            />
            <br/>
            <span>{messagePassword}</span>
          </div>
          <button onClick={this.handleLogin} className="btn btn-lg btn-primary btn-block btn-signin">Entrar</button>
        </form>
      </div>
    );
  }
}
