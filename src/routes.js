import React from 'react';
import { BrowserRouter as Router, Route } from 'react-router-dom';
import firebase from 'firebase';
import Login from './containers/Login';
import Page from './containers/Page';

firebase.initializeApp({
  apiKey: "AIzaSyBpUbx93tJfgt5-OO6-bxXkfV2WXnYP6-A",
  authDomain: "app-facebook-9371d.firebaseapp.com",
  databaseURL: "https://app-facebook-9371d.firebaseio.com",
  projectId: "app-facebook-9371d",
  storageBucket: "",
  messagingSenderId: "808432349007"
});

class Routes extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      user: null
    };
  }

  componentDidMount() {
    firebase.auth().onAuthStateChanged(user => {
      if (user) {
        this.setState({ user });
      } else {
        this.setState({ user: null });
      }
    });
  }

  render() {
    let Component = Login;
    if (this.state.user)
      Component = Page;
    return (
      <Router {...this.props}>
        <div>
          <Route path='/' render={() => {
            return <Component firebase={firebase} />
          }} />
        </div>
      </Router>
    )
  }
}

export default Routes;
