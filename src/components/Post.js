import React, { Component } from 'react';
import profile from '../assets/img/profile.jpg';

export default class Post extends Component {
	constructor(props){
		super(props);
		this.delete = this.delete.bind(this);
	}

	delete() {
		this.props.delete(this.props.item.id);
	}

	render() {
		
		const {
			description,
		} = this.props.item;

		return (
			<div className="panel panel-default">
				<button onClick={this.delete} type="button" className="dotbtn dropdown-toggle pull-right" data-toggle="dropdown" aria-expanded="false"> <span className="dots"></span> </button>
				<div className="col-md-12">
					<div className="media">
						<div className="media-left">
							<a>
								<img src={profile} alt="" className="media-object" />
							</a>
						</div>
						<div className="media-body">
							<h4 className="media-heading">Paola Ortiz de Zevallos<br />
							<small><i className="fa fa-clock-o"></i> Yesterday, 2:00 am</small> </h4>
							<p>{description}</p>
							<ul className="nav nav-pills pull-left ">
								<li><a title=""><i className="glyphicon glyphicon-thumbs-up"></i> 2015</a></li>
								<li><a title=""><i className=" glyphicon glyphicon-comment"></i> 25</a></li>
								<li><a title=""><i className="glyphicon glyphicon-share-alt"></i> 15</a></li>
							</ul>
						</div>
					</div>
				</div>
			</div>
		);
	}
}

