import React, { Component } from 'react';
import Header from '../components/Header';
import Panel from '../components/Panel';
import FormPost from '../components/forms/FormPost';
import profile from '../assets/img/profile.jpg';
import friend from '../assets/img/friend.jpg';
import contact from '../assets/img/contact.jpg';
import '../assets/styles/custom.css';

class Page extends Component {
	constructor(props) {
		super(props);
		this.handleLogOut = this.handleLogOut.bind(this);
	}

	handleLogOut() {
		this.props.firebase.auth().signOut()
			.catch(error => console.log(error))
	}

	render() {
		return (
			<div className="container">
				<div className="row">
					<div className="col-md-12">
						<button onClick={this.handleLogOut}>LogOut</button>
					</div>
					<div className="col-md-12 text-center">
						<Header />
					</div>
					<Panel />
					<div className="col-md-8 col-sm-12 pull-left posttimeline">
						<div className="panel panel-default">
							<div className="panel-body">
								<div className="status-upload nopaddingbtm">
									<FormPost />
								</div>
							</div>
						</div>

					</div>
				</div>
			</div>
		);
	}
}

export default Page;
