import React, { Component } from 'react';

export default class Input extends Component{

  render(){
    const {
      className, name, id, placeholder,
      value, type, onChange,
    } = this.props;
    return (
      <input
        className={className}
        name={name}
        id={id}
        placeholder={placeholder}
        value={value}
        type={type}
        onChange={onChange}
				required
      />
    )
  }
}