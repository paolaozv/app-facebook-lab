import React, { Component } from 'react';
import FormLogin from '../components/forms/FormLogin';
import FormGoogle from '../components/forms/FormGoogle';
import '../assets/styles/custom.css';

const Login = (props) => (
  <div className="contenedor">
    <div className="icon">
      <span className="glyphicon glyphicon-user"/>
    </div>
    <div className="contentForm">
      <FormLogin />
      <FormGoogle firebase={props.firebase} />
    </div>
  </div>
)

export default Login;
