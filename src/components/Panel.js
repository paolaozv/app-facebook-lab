import React, { Component } from 'react';
import profile from '../assets/img/profile.jpg';

export default class Panel extends Component {
	render() {
    return (
			<div className="col-md-4 col-sm-12 pull-right">
				<div className="panel panel-default">
					<div className="panel-heading">
						<h1 className="page-header small">Personal Details</h1>
						<p className="page-subtitle small">Limited information is visible</p>
					</div>
					<div className="col-md-12 photolist">
						<div className="row">
							<div className="col-sm-3 col-xs-3"><img src={profile} alt="" /> </div>
							<div className="col-sm-3 col-xs-3"><img src={profile} alt="" /> </div>
							<div className="col-sm-3 col-xs-3"><img src={profile} alt="" /> </div>
							<div className="col-sm-3 col-xs-3"><img src={profile} alt="" /> </div>
						</div>
					</div>
					<div className="clearfix"></div>
				</div>
				<div className="panel panel-default">
					<div className="panel-heading">
						<h1 className="page-header small">Worked with many domain</h1>
						<p className="page-subtitle small">Like to work fr different business</p>
					</div>
					<div className="col-md-12">
						<ul className="list-group">
							<li className="list-group-item"><span className="fa fa-male"></span> Worked with 1000+ people</li>
							<li className="list-group-item"><span className="fa fa-institution"></span> 60+ offices</li>
							<li className="list-group-item"><span className="fa fa-user"></span> 50000+ satify customers</li>
							<li className="list-group-item"><span className="fa fa-clock-o"></span> Work hours many and many still counting</li>
							<li className="list-group-item"><span className="fa fa-heart"></span> Customer satisfaction for servics</li>
						</ul>
					</div>
					<div className="clearfix"></div>
				</div>
			</div>
		);
	}
}