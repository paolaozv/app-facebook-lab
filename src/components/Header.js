import React, { Component } from 'react';
import profile from '../assets/img/profile.jpg';

export default class Header extends Component {
	render() {
    return (
			<div className="panel panel-default">
				<div className="userprofile social ">
					<div className="userpic">
						<img src={profile} className="userpicimg" alt=""/> 
					</div>
					<h3 className="username">Paola Ortiz de Zevallos</h3>
					<p>Lima, Perú</p>
					<div className="socials tex-center"> 
						<a className="btn btn-circle btn-primary">
							<i className="fa fa-facebook" />
						</a>
						<a className="btn btn-circle btn-danger">
							<i className="fa fa-google-plus" />
						</a>
						<a className="btn btn-circle btn-info">
							<i className="fa fa-twitter" />
						</a>
						<a className="btn btn-circle btn-warning">
							<i className="fa fa-envelope" />
						</a>
					</div>
				</div>
				<div className="col-md-12 border-top border-bottom">
					<ul className="nav nav-pills pull-left countlist" role="tablist">
						<li role="presentation">
							<h3>1452<br />
								<small>Follower</small>
							</h3>
						</li>
						<li role="presentation">
							<h3>245
								<br />
								<small>Following</small>
							</h3>
						</li>
						<li role="presentation">
							<h3>5000
								<br />
								<small>Activity</small>
							</h3>
						</li>
					</ul>
					<button className="btn btn-primary followbtn">Follow</button>
				</div>
				<div className="clearfix"></div>
			</div>
		);
	}
}